import { FC } from "react";
import { Helmet } from "react-helmet";
import { BrowserRouter, Routes, Route } from "react-router-dom";

// Views
import Views from "./views";

const App: FC = () => {
  return (
    <>
      <Helmet defaultTitle="Berlatih HTML" titleTemplate="%s | Berlatih HTML" />

      <BrowserRouter>
        <Routes>
          <Route path="/*" element={<Views />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
