import { FC } from "react";
import { useNavigate } from "react-router-dom";
import {
  useTheme,
  Typography,
  Grid,
  TextField,
  MenuItem,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  FormGroup,
  Checkbox,
  Button,
} from "@mui/material";
import { Country } from "country-state-city";

const FormView: FC = () => {
  const navigate = useNavigate();
  const theme = useTheme();

  return (
    <>
      <Typography variant="h4" fontWeight="bold" gutterBottom>
        Buat Account Baru
      </Typography>
      <Typography variant="h5" fontWeight="bold" gutterBottom>
        Sign Up Form
      </Typography>

      <div style={{ marginTop: theme.spacing(4) }}>
        <Grid container direction="column" spacing={[0, 2]}>
          <Grid item>
            <TextField fullWidth label="First Name" />
          </Grid>
          <Grid item>
            <TextField fullWidth label="Last Name" />
          </Grid>

          <Grid item>
            <FormControl>
              <FormLabel>Gender</FormLabel>
              <RadioGroup>
                <FormControlLabel
                  value="female"
                  control={<Radio />}
                  label="Female"
                />
                <FormControlLabel
                  value="male"
                  control={<Radio />}
                  label="Male"
                />
              </RadioGroup>
            </FormControl>
          </Grid>

          <Grid item>
            <TextField fullWidth select label="Nationality">
              {Country.getAllCountries().map((country) => (
                <MenuItem key={country.isoCode} value={country.name}>
                  {country.name}
                </MenuItem>
              ))}
            </TextField>
          </Grid>

          <Grid item>
            <FormControl>
              <FormLabel>Language Spoken</FormLabel>
              <FormGroup>
                <FormControlLabel
                  label="Bahasa Indonesia"
                  control={<Checkbox />}
                />
                <FormControlLabel label="English" control={<Checkbox />} />
                <FormControlLabel label="Other" control={<Checkbox />} />
              </FormGroup>
            </FormControl>
          </Grid>

          <Grid item>
            <TextField fullWidth multiline rows={5} label="Bio" />
          </Grid>

          <Grid item>
            <Button
              fullWidth
              variant="contained"
              size="large"
              onClick={() => navigate("/welcome")}
            >
              Sign Up
            </Button>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default FormView;
