import { lazy, Suspense, FC } from "react";
import { Routes, Route } from "react-router-dom";
import { useTheme, LinearProgress, Container } from "@mui/material";

const HomeView = lazy(() => import("./HomeView"));
const FormView = lazy(() => import("./FormView"));
const WelcomeView = lazy(() => import("./WelcomeView"));

const Views: FC = () => {
  const theme = useTheme();

  return (
    <Suspense fallback={<LinearProgress />}>
      <Container
        sx={{
          py: theme.spacing(4),
        }}
      >
        <Routes>
          <Route path="/" element={<HomeView />} />
          <Route path="/form" element={<FormView />} />
          <Route path="/welcome" element={<WelcomeView />} />
        </Routes>
      </Container>
    </Suspense>
  );
};

export default Views;
