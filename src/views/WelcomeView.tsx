import { FC } from "react";
import { Typography } from "@mui/material";

const WelcomeView: FC = () => (
  <>
    <Typography variant="h4" fontWeight="bold" gutterBottom>
      SELAMAT DATANG!
    </Typography>
    <Typography variant="h5" fontWeight="bold">
      Terima kasih telah bergabung di website kami. Media belajar kita bersama!
    </Typography>
  </>
);

export default WelcomeView;
