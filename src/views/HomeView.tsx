import { FC } from "react";
import {
  Typography,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Link,
} from "@mui/material";
import { RadioButtonUnchecked } from "@mui/icons-material";

const HomeView: FC = () => (
  <>
    <Typography variant="h4" fontWeight="bold" gutterBottom>
      Media Online
    </Typography>

    <Typography variant="h5" fontWeight="bold" gutterBottom>
      Sosial Media Developer
    </Typography>
    <Typography variant="body1" gutterBottom>
      Belajar dan berbagi agar hidup menjadi lebih baik.
    </Typography>

    <Typography variant="h6" fontWeight="bold" gutterBottom>
      Benefit Join di Media Online
    </Typography>
    <List dense>
      <ListItem>
        <ListItemIcon>
          <RadioButtonUnchecked />
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography variant="body1">
              Mendapatkan motivasi dari sesama para developer
            </Typography>
          }
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <RadioButtonUnchecked />
        </ListItemIcon>
        <ListItemText
          primary={<Typography variant="body1">Sharing knowledge</Typography>}
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <RadioButtonUnchecked />
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography variant="body1">
              Dibuat oleh calon web developer terbaik
            </Typography>
          }
        />
      </ListItem>
    </List>

    <Typography variant="h6" fontWeight="bold" gutterBottom>
      Cara Bergabung ke Media Online
    </Typography>
    <List dense>
      <ListItem>
        <ListItemIcon>
          <RadioButtonUnchecked />
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography variant="body1">Mengunjungi website ini</Typography>
          }
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <RadioButtonUnchecked />
        </ListItemIcon>
        <ListItemText
          primary={
            <Typography variant="body1">
              Mendaftarkan di{" "}
              <Link href="/form" target="_blank">
                Form Sign Up
              </Link>
            </Typography>
          }
        />
      </ListItem>
      <ListItem>
        <ListItemIcon>
          <RadioButtonUnchecked />
        </ListItemIcon>
        <ListItemText
          primary={<Typography variant="body1">Selesai</Typography>}
        />
      </ListItem>
    </List>
  </>
);

export default HomeView;
